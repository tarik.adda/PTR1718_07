
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"


#include "basic_io.h"

unsigned long ulTaskNumber[configEXPECTED_NO_RUNNING_TASKS];


static void vSenderTask( void *pvParameters );
static void vReceiverTask( void *pvParameters );


xQueueHandle xQueue;

typedef struct {
    int variable;
    int id;
}xVal_t;

		xVal_t t1 = {100,1};
		xVal_t t2 = {200,2};

int main( void )
{
vPrintStringAndNumber( "Premier test = ", t1.id );
	
    xQueue = xQueueCreate( 5, sizeof( xVal_t ) );

	if( xQueue != NULL )
	{

		xTaskCreate( vSenderTask, "1", 240, ( void * ) &t1, 2, NULL );
		xTaskCreate( vSenderTask, "2", 240, ( void * ) &t2, 2, NULL );

		xTaskCreate( vReceiverTask, "Receiver", 240, NULL, 1, NULL );

		vTaskStartScheduler();
	}
	else
	{

	}
		
	for( ;; );
}

static void vSenderTask(void *pvParameters )
{
const portTickType xTicksToWait = 100 / portTICK_RATE_MS;

portBASE_TYPE xStatus;

	for( ;; )
	{
		xStatus = xQueueSendToBack( xQueue, pvParameters, xTicksToWait );

		
		if( xStatus != pdPASS )
		{
			vPrintString( "Could not send to the queue.\r\n" );
		}

		taskYIELD();
	}
}

static void vReceiverTask( void *pvParameters )
{
xVal_t t_final;
portBASE_TYPE xStatus;


	for( ;; )
	{
		if( uxQueueMessagesWaiting( xQueue ) != 0 )
		{
			vPrintString( "Queue should have been empty!\r\n" );
		}

		xStatus = xQueueReceive( xQueue, &t_final, 0 );

		if( xStatus == pdPASS )
		{
			if(t_final.id == 1){
			  vPrintStringAndNumber( "Received from 1 = ", t_final.variable );
		  }else{
			  vPrintStringAndNumber( "Received from 2 =  ", t_final.variable );
			}
		}
		else
		{
			vPrintString( "Could not receive from the queue.\r\n" );
		}
	}
}

void vApplicationMallocFailedHook( void )
{
	for( ;; );
}

void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed char *pcTaskName )
{
	for( ;; );
}

void vApplicationIdleHook( void )
{

}


void vApplicationTickHook( void )
{

}


