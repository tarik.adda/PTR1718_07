#include "FreeRTOS.h"
#include "task.h"

#include "basic_io.h"

#define mainDELAY_LOOP_COUNT		(0xfffff)

void vTacheexec(void *pvParameters);

static const char *T1 = "Task 1 is running\r\n";
static const char *T2 = "Task 2 is running\r\n";

//void vTask1(void *pvParameters);
//void vTask2(void *pvParameters);

unsigned long ulTaskNumber[configEXPECTED_NO_RUNNING_TASKS];

int main(void)
{
	
	xTaskCreate(vTacheexec, "Tache 1", 200, (void*)T1, 1, NULL);
	
	xTaskCreate(vTacheexec, "Tache 2", 200, (void*)T2, 1, NULL);
	
	/*
  xTaskCreate(vTask1, "Task 1", 200, NULL, 1, NULL);

  xTaskCreate(vTask2, "Task 2", 200, NULL, 1, NULL);
*/
  vTaskStartScheduler();

  for(;;);
}

void vTacheexec(void *pvParameters){
		const char* pcTaskName = ( char * ) pvParameters;

  volatile unsigned long ul;

  for(;;) {
    vPrintString(pcTaskName);

    for(ul = 0; ul < mainDELAY_LOOP_COUNT; ul++) {

    }
  }
	
}

/*
void vTask1(void *pvParameters)
{
  vTacheexec(0);
}

void vTask2(void *pvParameters)
{
	vTacheexec(1);
}
*/