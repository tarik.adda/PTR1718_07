#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "basic_io.h"

#include "ARMCM3.h"
#include "queue.h"
#include "timers.h"

#define mainTRIGGER_INTERRUPT()	NVIC_SetPendingIRQ( mainSW_INTERRUPT_ID )

#define mainCLEAR_INTERRUPT()	NVIC_ClearPendingIRQ( mainSW_INTERRUPT_ID )

#define mainSOFTWARE_INTERRUPT_PRIORITY 		( 5 )

#define NUM_TIMERS 3

TimerHandle_t xTimers;

unsigned long ulTaskNumber[configEXPECTED_NO_RUNNING_TASKS];
uint32_t MessageSended = 100;


static void vSenderTask( void *pvParameters );//Tache 1
static void vReactionTask( void *pvParameters );//Tache 2

static void vProcessEventNotifiedBySemaphore(void); //Quand Tache 1 a le semaphore
static void vProcessValueFromQueue1(void* MessageSended); //Quand Tache 1 a la value

//Differentes variables
xQueueHandle xQueue, xBinarySemaphore;
static QueueSetHandle_t xQueueSet;
QueueSetMemberHandle_t xActivatedMember;

//Callback du timer
void vTimerCallback( TimerHandle_t xTimer )
 {
		vPrintString( "Timer Semaphore OK.\r\n" );
	 //Donnes la semaphore
		xSemaphoreGive(xBinarySemaphore);
 }


int main( void )
{
	//Timer lancer avec le callback
  xTimers= xTimerCreate("Timer",portTICK_RATE_MS * 100,pdTRUE,(void * ) 0,vTimerCallback);
	xTimerStart(xTimers, 0 );

	//Creation du QueueSet
	xQueue = xQueueCreate( 5, sizeof( MessageSended ) );
	xQueueSet = xQueueCreateSet( sizeof( xQueue ) + sizeof( xBinarySemaphore ));
	xBinarySemaphore = xSemaphoreCreateBinary();
		 
	//Verifs
	configASSERT( xQueueSet );
  configASSERT( xQueue );
  configASSERT( xBinarySemaphore );
	
	//Initialisation du QueueSet
	xQueueAddToSet( xQueue, xQueueSet );
  xQueueAddToSet( xBinarySemaphore, xQueueSet );	 
	
	//Creation Tache
	xTaskCreate( vSenderTask, "Sender1", 240, (void *) MessageSended, 1, NULL );
	xTaskCreate( vReactionTask, "Reaction", 240, NULL, 1, NULL );
	//Demarrage timer
	vTaskStartScheduler();
	for( ;; );
}

static void vReactionTask(void* pvParameters)
{
	for( ;; )
    {
			//Recuperation du queueSet
				xActivatedMember = xQueueSelectFromSet( xQueueSet,portMAX_DELAY);

        if( xActivatedMember == xQueue )
        {
					//Si on a la valeur
            xQueueReceive( xActivatedMember, &MessageSended, 0 );
            vProcessValueFromQueue1( (void*) MessageSended );
        }
        else if( xActivatedMember == xBinarySemaphore )
        {
					//Si on a le semaphore
            /* Take the semaphore to make sure it can be "given" again. */
            xSemaphoreTake( xActivatedMember, 0 );
            vProcessEventNotifiedBySemaphore();
            break;
        }
		}
}

static void vProcessEventNotifiedBySemaphore(void)
{
	//Semaphore prise
	vPrintString( "Event Semaphore OK.\r\n" );
}

static void vProcessValueFromQueue1(void* MessageSended)
{
	  uint32_t MessageSend; 
		portBASE_TYPE xStatus;
	  const portTickType xTicksToWait = 100 / portTICK_RATE_MS;
		xStatus = xQueueReceive( xQueue, &MessageSend,  xTicksToWait );

		if( xStatus == pdPASS )
		{
			vPrintStringAndNumber( "Received = ", MessageSend);
		}
		else
		{
			vPrintString( "Could not receive from the queue.\r\n" );
		}
	vPrintString( "Event Queue Messsage OK.\r\n" );
}


static void vSenderTask(void* pvParameters)
{
long lValueToSend;
portBASE_TYPE xStatus;
const portTickType xTicksToWait = 100 / portTICK_RATE_MS;
lValueToSend = ( long ) pvParameters;
	
	for( ;; )
	{
		xStatus = xQueueSendToBack( xQueue, &lValueToSend, xTicksToWait );

		if( xStatus != pdPASS )
		{
			vPrintString( "Could not send to the queue.\r\n" );
		}
		taskYIELD();
	}
}









