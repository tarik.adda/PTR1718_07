
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#include "basic_io.h"

#define NUM_TIMERS 2

unsigned long ulTaskNumber[configEXPECTED_NO_RUNNING_TASKS];
TimerHandle_t xTimers[ NUM_TIMERS ];
unsigned long ulLed[2] = {0, 0};

void vTimerCallback( TimerHandle_t xTimer )
 {
	  uint32_t ulCount;
		ulCount = ( uint32_t ) pvTimerGetTimerID( xTimer );
	 
	 if (ulLed[ulCount] == 0)
	 {
		 ulLed[ulCount] = 1;
	 }
	 else
	 {
		 ulLed[ulCount] = 0;
	 }
 }

 
int main (void )
 {
		long x;
     
		for( x = 0; x < NUM_TIMERS; x++ )
			{
         xTimers[ x ] = xTimerCreate("Timer", ((1+x)*500) / portTICK_RATE_MS, pdTRUE, ( void * ) x, vTimerCallback);

         if( xTimers[ x ] == NULL )
         {
             vPrintString( "The timer was not created.\n" );
         }
         else
         {
             if( xTimerStart( xTimers[ x ], 0 ) != pdPASS )
             {
                 vPrintString("The timer could not be set into the Active state.\n");
             }
         }
     }

     vTaskStartScheduler();
     for( ;; );
 }


/*
void vTaskFunction(void *pvParameters)
{
  char *pcTaskName;
  portTickType xLastWakeTime;
  pcTaskName = ( char * ) pvParameters;
  xLastWakeTime = xTaskGetTickCount();

  for(;;)
	{
    vPrintString( pcTaskName );
    vTaskDelayUntil( &xLastWakeTime, ( 250 / portTICK_RATE_MS ) );
  }
}
*/
 
 

