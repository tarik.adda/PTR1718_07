
#include "FreeRTOS.h"
#include "task.h"

#include "basic_io.h"

#define GPIO0_DIR            				(*(unsigned long*)0x2009C000)
#define GPIO0_SET            				(*(unsigned long*)0x2009C018)
#define GPIO0_CLR            				(*(unsigned long*)0x2009C01C)
	
void vTaskFunction( void *pvParameters );

unsigned long ulTaskNumber[configEXPECTED_NO_RUNNING_TASKS];

const char *pcTextForTask1 = "Task 1 is running\n";
const char *pcTextForTask2 = "Task 2 is running\n";

volatile uint32_t ulIdleCycleCount = 0UL;

int main(void)
{
	GPIO0_DIR=0x7;
  xTaskCreate( vTaskFunction, "Task 1", 240, (void*)pcTextForTask1, 1, NULL );

  xTaskCreate( vTaskFunction, "Task 2", 240, (void*)pcTextForTask2, 2, NULL );

  vTaskStartScheduler();	
	
  for(;;);
}

void vTaskFunction(void *pvParameters)
{
  char *pcTaskName;
  portTickType xLastWakeTime;

  pcTaskName = ( char * ) pvParameters;

  xLastWakeTime = xTaskGetTickCount();

  for(;;)	{
    vPrintString( pcTaskName );
    vTaskDelayUntil( &xLastWakeTime, ( 250 / portTICK_RATE_MS ) );
  }
}


void vApplicationIdleHook( void )
{
 //vPrintStringAndNumber("Idle en route",ulTaskNumber[1]);
 ulIdleCycleCount++;
}
